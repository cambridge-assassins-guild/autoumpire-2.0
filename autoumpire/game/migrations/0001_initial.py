import django.core.validators
import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Assassin',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='The name by which the Assassin is known by', max_length=255,
                                          verbose_name='player name')),
            ],
        ),
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(
                    help_text='A name referencing the game theme e.g. &ldquo;20<sup>th</sup> Century Sci-Fi&rdquo;',
                    max_length=255)),
                ('year', models.PositiveSmallIntegerField()),
                ('active', models.BooleanField(help_text='Is this game active?')),
                ('begin_at', models.DateTimeField(help_text='When the game begins', null=True)),
                ('end_at', models.DateTimeField(help_text='When the game ends', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='GameType',
            fields=[
                ('name',
                 models.CharField(help_text='e.g. Michaelmas, Lent, Easter, Varsity', max_length=255, primary_key=True,
                                  serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='MainGamePlayer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('notes', models.TextField(help_text='Other details about the player that should be made public')),
                ('water_status',
                 models.TextField(choices=[('NO', 'no water'), ('WC', 'water with care'), ('FW', 'full water')],
                                  max_length=2)),
                ('is_alive', models.BooleanField(default=True, help_text='Whether this player is alive')),
                ('competency_deadline', models.DateTimeField(help_text="When this player's competency expires")),
                ('address', models.TextField(help_text='A room address, or details allowing the player to be found')),
                ('score', models.DecimalField(decimal_places=2, default=0.0, max_digits=6,
                                              validators=[django.core.validators.MinValueValidator(0.0)])),
                ('assassin', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='game.assassin')),
                ('game', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='game.game')),
                ('targets', models.ManyToManyField(to='game.MainGamePlayer')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PolicePlayer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('notes', models.TextField(help_text='Other details about the player that should be made public')),
                ('water_status',
                 models.TextField(choices=[('NO', 'no water'), ('WC', 'water with care'), ('FW', 'full water')],
                                  max_length=2)),
                ('respawn_at', models.DateTimeField(verbose_name='respawn at')),
                ('assassin', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='game.assassin')),
                ('game', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='game.game')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Pronoun',
            fields=[
                ('short_text',
                 models.CharField(help_text='A short user-facing string e.g. &ldquo;they/them&rdquo;', max_length=255,
                                  primary_key=True, serialize=False)),
                ('subject_pronoun',
                 models.CharField(help_text='e.g. &ldquo;<em>He/She/They</em> has a Nerf gun&rdquo;', max_length=7)),
                ('object_pronoun',
                 models.CharField(help_text='e.g. &ldquo;I saw <em>him/her/them</em> in the lecture hall&rdquo;',
                                  max_length=7)),
                ('dep_possessive_pronoun',
                 models.CharField(help_text='e.g. &ldquo;In <em>his/her/their</em> room&rdquo;', max_length=7,
                                  verbose_name='Dependent possessive pronoun')),
                ('indep_possessive_pronoun',
                 models.CharField(help_text='e.g. &ldquo;It was <em>his/hers/theirs</em> for the taking&rdquo;',
                                  max_length=7, verbose_name='Independent possessive pronoun')),
                ('reflexive_pronoun',
                 models.CharField(help_text='e.g. &ldquo;All by <em>himself/herself/themself</em>&rdquo;',
                                  max_length=7)),
            ],
        ),
        migrations.CreateModel(
            name='Rank',
            fields=[
                ('rank',
                 models.CharField(help_text='e.g. &ldquo;Biker Gang Overlord&rdquo;', max_length=255, primary_key=True,
                                  serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='PolicePseudonym',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name',
                 models.CharField(help_text='e.g. &ldquo;The Ravenous Bugblatter Beast of Traal&rdquo;', max_length=255,
                                  verbose_name='pseudonym')),
                ('game_player', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='game.policeplayer')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='policeplayer',
            name='rank',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='game.rank'),
        ),
        migrations.CreateModel(
            name='PlayerPseudonym',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name',
                 models.CharField(help_text='e.g. &ldquo;The Ravenous Bugblatter Beast of Traal&rdquo;', max_length=255,
                                  verbose_name='pseudonym')),
                ('game_player',
                 models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='game.maingameplayer')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='game',
            name='game_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='game.gametype'),
        ),
        migrations.AddField(
            model_name='assassin',
            name='pronouns',
            field=models.ForeignKey(default='they/them', on_delete=django.db.models.deletion.PROTECT,
                                    to='game.pronoun'),
        ),
    ]
