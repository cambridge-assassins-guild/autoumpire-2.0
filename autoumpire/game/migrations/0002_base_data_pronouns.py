from django.apps import apps as app_registry
from django.db import migrations
from django.db.backends.base.schema import BaseDatabaseSchemaEditor


def populate_pronouns(apps: app_registry, _schema_editor: BaseDatabaseSchemaEditor):
    pronoun = apps.get_model("game", "Pronoun")

    def register(short, subj, obj, dpos, ipos, refl):
        p = pronoun(
            short_text=short,
            subject_pronoun=subj,
            object_pronoun=obj,
            dep_possessive_pronoun=dpos,
            indep_possessive_pronoun=ipos,
            reflexive_pronoun=refl
        )
        p.save()

    register("he/him", "he", "him", "his", "his", "himself")
    register("she/her", "she", "her", "her", "hers", "herself")
    register("they/them", "they", "them", "their", "theirs", "themself")


class Migration(migrations.Migration):
    dependencies = [
        ('game', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(populate_pronouns)
    ]
