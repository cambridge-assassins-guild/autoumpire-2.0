from django.apps import apps as app_registry
from django.db import migrations
from django.db.backends.base.schema import BaseDatabaseSchemaEditor


def populate_gametypes(apps: app_registry, _schema_editor: BaseDatabaseSchemaEditor) -> None:
    game_type = apps.get_model("game", "GameType")

    def register(name: str) -> None:
        p = game_type(name=name)
        p.save()

    from ..models import GameType
    register(GameType.MICHAELMAS)
    register(GameType.LENT)
    register(GameType.EASTER)


class Migration(migrations.Migration):
    dependencies = [
        ('game', '0002_base_data_pronouns'),
    ]

    operations = [
        migrations.RunPython(populate_gametypes)
    ]
