from django.contrib import admin

from .models.players import MainGamePlayer, PolicePlayer
from .models.humans import Assassin, Pronoun
from .models.game import Game, GameType

admin.site.register(Game)
admin.site.register(GameType)
admin.site.register(Assassin)
admin.site.register(Pronoun)
admin.site.register(MainGamePlayer)
admin.site.register(PolicePlayer)
