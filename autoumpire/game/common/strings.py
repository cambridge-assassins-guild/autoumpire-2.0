def html_em(s: str) -> str:
    """
    Returns its argument between HTML emphasis tags.
    Does not escape its input.
    """
    return _html_tag("em", s)


def html_sup(s: str) -> str:
    """
    Returns its argument between HTML superscript tags.
    Does not escape its input.
    """
    return _html_tag("sup", s)


def html_quote(s: str) -> str:
    """
    Returns its argument between HTML double-quotes.
    Does not escape its input.
    """
    return f"&ldquo;{s}&rdquo;"


def _html_tag(tag: str, inner: str) -> str:
    return f"<{tag}>{inner}</{tag}>"
