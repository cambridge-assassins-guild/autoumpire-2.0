from __future__ import annotations
from django.core.validators import MinValueValidator
from django.db import models
from django.db.models import QuerySet

from ..common.strings import html_quote


class AbstractPseudonym(models.Model):
    """AbstractPseudonym represents a pseudonym used by an assassin during the game."""
    name = models.CharField(
        max_length=255, verbose_name="pseudonym",
        help_text=f"e.g. {html_quote('The Ravenous Bugblatter Beast of Traal')}"
    )

    class Meta:
        abstract = True

    def __str__(self) -> str:
        return self.name


class AbstractGamePlayer(models.Model):
    """
    A GamePlayer is an Assassin in a given Game.
    This is the abstract base class from which each type of Player is derived.
    """

    class Meta:
        abstract = True

    class WaterStatus(models.TextChoices):
        """WaterStatus describes what this GamePlayer's preferences are around water weapons."""
        NO_WATER = "NO", "no water"
        WATER_WITH_CARE = "WC", "water with care"
        FULL_WATER = "FW", "full water"

    # TODO: If an Assassin is deleted from the system, replace them with an Assassin called "[deleted]".
    #       This should be done in a way that maintains scores and other attributes of the player.
    #       Alternately, s/deletion/update data corresponding to that person.
    assassin = models.ForeignKey("Assassin", on_delete=models.PROTECT)

    # protected as games should not be deleted
    # TODO: admin interface default value
    game = models.ForeignKey("Game", on_delete=models.PROTECT)

    notes = models.TextField(
        help_text="Other details about the player that should be made public"
    )
    water_status = models.TextField(
        max_length=2, choices=WaterStatus.choices,
    )

    def __str__(self) -> str:
        return f"{self.assassin.name} ({self.assassin.pronouns})"


class MainGamePlayer(AbstractGamePlayer):
    """
    A MainGamePlayer is a GamePlayer that is in the main game.
    """

    is_alive = models.BooleanField(default=True, help_text="Whether this player is alive")
    competency_deadline = models.DateTimeField(help_text="When this player's competency expires")
    address = models.TextField(
        help_text="A room address, or details allowing the player to be found"
    )
    # Stores an exact decimal in the form NNNN.NN
    score = models.DecimalField(
        max_digits=6, decimal_places=2, default=0.0, validators=[MinValueValidator(0.0)]
    )
    # Core of the targeting algorithm: a relation between MainGamePlayers reflecting who is assigned as a target of
    # another assassin. Per the design docs, this is not reflexive: we like the surprise factor of not knowing your
    # potential assassin.
    targets = models.ManyToManyField("self", symmetrical=False)

    class PlayerPseudonym(AbstractPseudonym):
        game_player = models.ForeignKey("MainGamePlayer", on_delete=models.CASCADE)

    def get_latest_pseudonym(self) -> AbstractPseudonym:
        """Returns the most recently used pseudonym used by this GamePlayer"""
        return self.playerpseudonym_set.order_by("-id").first()

    def get_all_pseudonyms(self) -> QuerySet[AbstractPseudonym]:
        """Returns all pseudonyms used by this GamePlayer, ordered first to last"""
        return self.playerpseudonym_set.order_by("id")

    def get_targets(self) -> QuerySet[MainGamePlayer]:
        return self.targets.get_queryset()

    def get_targeted_by(self) -> QuerySet[MainGamePlayer]:
        return self.maingameplayer_set.get_queryset()


class PolicePlayer(AbstractGamePlayer):
    """
    A PolicePlayer is a GamePlayer that is playing in the police game.
    If a MainGamePlayer is recruited to the police game after their death, the PolicePlayer is instantiated using their
    data, allowing them to change their pseudonym/s etc.
    """
    class PolicePseudonym(AbstractPseudonym):
        game_player = models.ForeignKey("PolicePlayer", on_delete=models.CASCADE)

    def get_latest_pseudonym(self) -> AbstractPseudonym:
        """Returns the most recently used pseudonym used by this GamePlayer"""
        return self.policepseudonym_set.order_by("-id").first()

    def get_all_pseudonyms(self) -> QuerySet[AbstractPseudonym]:
        """Returns all pseudonyms used by this GamePlayer, ordered first to last"""
        return self.policepseudonym_set.order_by("id")

    class Rank(models.Model):
        rank = models.CharField(
            max_length=255, primary_key=True,
            help_text=f"e.g. {html_quote('Biker Gang Overlord')}"
        )

    respawn_at = models.DateTimeField(verbose_name="respawn at")
    rank = models.ForeignKey("Rank", on_delete=models.PROTECT)  # protect as ranks should not be deleted
