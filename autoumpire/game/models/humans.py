from django.db import models
from ..common.strings import html_em, html_quote


class Assassin(models.Model):
    """
    Assassin contains the details for a particular player that persist between games.
    """
    name = models.CharField(
        max_length=255, verbose_name="player name",
        help_text="The name by which the Assassin is known by"
    )
    pronouns = models.ForeignKey("Pronoun", on_delete=models.PROTECT, default="they/them")

    def __str__(self) -> str:
        return self.name


class Pronoun(models.Model):
    """
    A Pronoun represents that particular part of speech.
    This could be an enum field but has been left open for extension.
    """
    short_text = models.CharField(
        primary_key=True, max_length=255,
        help_text=f"A short user-facing string e.g. {html_quote('they/them')}"
    )
    subject_pronoun = models.CharField(
        max_length=7,
        help_text="e.g. " + html_quote(f"{html_em('He/She/They')} has a Nerf gun")
    )
    object_pronoun = models.CharField(
        max_length=7,
        help_text="e.g. " + html_quote(f"I saw {html_em('him/her/them')} in the lecture hall")
    )
    dep_possessive_pronoun = models.CharField(
        max_length=7,
        verbose_name="Dependent possessive pronoun",
        help_text="e.g. " + html_quote(f"In {html_em('his/her/their')} room")
    )
    indep_possessive_pronoun = models.CharField(
        max_length=7,
        verbose_name="Independent possessive pronoun",
        help_text="e.g. " + html_quote(f"It was {html_em('his/hers/theirs')} for the taking")
    )
    reflexive_pronoun = models.CharField(
        max_length=7,
        help_text="e.g. %s" % html_quote(f"All by {html_em('himself/herself/themself')}")
    )

    def __str__(self) -> str:
        return self.short_text
