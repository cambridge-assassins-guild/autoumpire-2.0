from __future__ import annotations

from datetime import date
from typing import Final, Optional

from django.core.exceptions import ValidationError
from django.db import models

from ..common.strings import html_quote, html_sup


class GameType(models.Model):
    """
    A GameType represents a particular type of game, e.g. a Lent term game, or a Varsity game
    """
    MICHAELMAS: Final = "Michaelmas"
    LENT: Final = "Lent"
    EASTER: Final = "Easter"

    name = models.CharField(
        max_length=255, primary_key=True,
        help_text=f"e.g. {MICHAELMAS}, {LENT}, {EASTER}, Varsity"
    )

    @classmethod
    def suggest_gametype(cls, when: date = date.today()) -> Optional[GameType]:
        """
        Accepts a particular date, or uses the current date if unset.
        Returns the GameType for that Date, or None if outside of term months (July to October inclusive).
        Assumes there's three GameTypes called "Michaelmas", "Lent" and "Easter", but doesn't HCF if not.
        Warning: this can't be used to initialise a default value field!
        """
        term_name = cls.current_term_name(when)
        if term_name is None:
            return None

        try:
            return cls.objects.get(name=term_name)
        except cls.DoesNotExist:
            return None

    @classmethod
    def current_term_name(cls, when: date = date.today()) -> Optional[str]:
        """
        Accepts a particular date, or uses the current date if unset.
        Returns the name of the term this date falls within, or None if it falls outside of term.
        """
        if 1 <= when.month <= 3:
            # January, February, March
            return cls.LENT
        elif 4 <= when.month <= 6:
            # April, May, June
            return cls.EASTER
        elif 10 <= when.month <= 12:
            # October, November, December
            return cls.MICHAELMAS
        else:
            return None

    def is_date_valid(self, when: date) -> bool:
        """Returns a boolean indicating if the provided date is valid for this GameType."""
        if self.name == self.MICHAELMAS:
            return 10 <= when.month <= 12
        elif self.name == self.LENT:
            return 1 <= when.month <= 3
        elif self.name == self.EASTER:
            return 4 <= when.month <= 6
        else:
            return True

    def __str__(self) -> str:
        return self.name


class Game(models.Model):
    """
    A Game represents a particular Guild game.
    Games are not constrained to only have one per year and term, to provide future flexibility.
    """

    name = models.CharField(
        max_length=255,
        help_text="A name referencing the game theme e.g. " + html_quote(f"20{html_sup('th')} Century Sci-Fi")
    )
    # TODO: admin interface default values
    # TODO: update help text fields
    game_type = models.ForeignKey("GameType", on_delete=models.PROTECT)
    year = models.PositiveSmallIntegerField()
    active = models.BooleanField(help_text="Is this game active?")
    begin_at = models.DateTimeField(null=True, help_text="When the game begins")
    end_at = models.DateTimeField(null=True, help_text="When the game ends")

    @classmethod
    def suggest_current_game(cls, when: date = date.today()) -> Optional[Game]:
        """
        Attempts to find and return the current game, returning None if this determination isn't possible.
        """
        qs = cls.objects.filter(year=when.year)

        term_name = GameType.current_term_name(when)
        if term_name is not None:
            qs = qs.filter(game_type__name=term_name)

        return qs.order_by("-pk").first()

    def clean(self) -> None:
        # Validates the whole model for inconsistencies.

        if self.begin_at is not None and self.end_at is not None and self.end_at <= self.begin_at:
            raise ValidationError("Game must start before it ends")

        # If the game is active, begin_at and end_at must be set.
        if self.active:
            if self.begin_at is None:
                raise ValidationError("Game is active but doesn't have a start time")
            if self.end_at is None:
                raise ValidationError("Game is active but doesn't have an end time")

        # If begin/end_at are set, and the game takes place in a given term, both dates must fall in that term.
        if self.begin_at is not None and not self.game_type.is_date_valid(self.begin_at):
            raise ValidationError("Game starts outside of term")

        if self.end_at is not None and not self.game_type.is_date_valid(self.end_at):
            raise ValidationError("Game ends outside of term")

    def __str__(self) -> str:
        return f"{self.game_type.name} {self.year} ({self.name})"
