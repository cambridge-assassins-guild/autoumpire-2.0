# AutoUmpire 2

## About

The AutoUmpire 2 is a Django application that allows umpires of the Assassins' Guild to manage and run their termly games.

## Installation

These instructions assume you're hosting the AutoUmpire on the Student-Run Computing Facility infrastructure (in other words, a reasonably privileged shared hosting environment behind the Apache web server).

If it becomes necessary to migrate away from the SRCF's hosting, you'll need to set up an appropriate host running a compatible webserver. Contact your nearest available compsci for details.

TODO: this section.

TODO: configuration changes for development vs production.

## Development

The following steps assume some familiarity with Git, Django, and so on.

1. Install the latest version of [Python](https://www.python.org/).
    - On Windows, ensure that you've enabled the option to put Python on your PATH.
2. Install and configure [Git](https://git-scm.com/). Alternately, use a client specific to the repository host.
    - Ditto for Windows users: Git must be available on your PATH.
    - Git configuration is outside the scope of this tutorial, but at a minimum set `user.name` and `user.email` sensibly.
3. Use Git to clone the latest copy of this repository to your computer.
4. Open a terminal in the top directory of this repository, and issue the following commands:
    - Create and activate a virtual environment for the project: as per [the Python docs](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/).
    - Update pip: `pip install --upgrade pip`
    - Download and install dependencies: `pip install -r requirements.txt`
    - Change directory: `cd autoumpire`
    - Apply migrations to the Django database: `python manage.py migrate`

You should now be able to run the AU application locally with `python manage.py runserver`.

TODO: dev vs prod configs, unit testing, security testing, auth\[nz\], etc

## Making and committing changes

This repository is configured with a protected master branch. This ensures code is properly reviewed before being committed, and also helps ensure Django migrations are generated and applied correctly.

First, make sure you're working on the latest version of the repository; then, setup a feature branch:

```
git checkout main
git pull
git checkout -b [your CRSid]/[a descriptive branch name]
```

Make your changes and use `git add`, `git commit`, and `git push` to upload them to the repo. Use descriptive commit messages in the present-tense imperative style (e.g. "add feature X", "migrate to Y", "fix issue #n"). When ready, open a merge request in GitLab and seek feedback.
